//
//  SetlistGrabber.h
//  PhotoBrowser
//
//  Created by Andrew Lloyd on 31/03/2014.
//  Copyright (c) 2014 Andrew Lloyd. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SetlistGrabber : NSObject <NSXMLParserDelegate>

@property (nonatomic, strong) NSManagedObjectContext *managedObjectContext;

@property(nonatomic, strong)NSArray *setlistSongs;
@property(nonatomic, strong)NSDictionary *setlists;
@property(nonatomic, strong)NSString *returnedSetlistDate;
@property(nonatomic, strong)NSString *returendSetlistVenue;

-(void)fetchSetlistofArtist:(NSString*)artistName atVenue:(NSString*)venueName onDate:(NSString*)date inContext:(NSManagedObjectContext*)context;

@end
