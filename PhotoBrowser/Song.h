#import "_Song.h"

@interface Song : _Song {}
// Custom logic goes here.

- (void)populateWithJSON:(NSDictionary*)json inContext:(NSManagedObjectContext*)context;

@end
