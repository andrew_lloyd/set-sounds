//
//  SpotifyServices.h
//  PhotoBrowser
//
//  Created by Andrew Lloyd on 21/10/2015.
//  Copyright © 2015 Andrew Lloyd. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Spotify/Spotify.h>
#import "Setlist.h"

@interface SpotifyServices : NSObject

@property (nonatomic, strong) SPTAudioStreamingController *player;

-(void)createPlaylistForSetWithSetlist:(Setlist *)setlist
                           withSuccess:(void (^) ())success
                            andFailure:(void (^) (NSError *error))failure;

-(void)fetchSongURIsForSetlist:(Setlist*)setlist
                   withSuccess:(void (^) ())success
                    andFailure:(void (^) (NSError *error))failure;

@end
