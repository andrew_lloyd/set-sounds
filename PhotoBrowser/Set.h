#import "_Set.h"

@interface Set : _Set {}
// Custom logic goes here.
- (void)populateWithJSON:(NSDictionary*)json inContext:(NSManagedObjectContext*)context;

@end
