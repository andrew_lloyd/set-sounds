// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to Song.h instead.

#import <CoreData/CoreData.h>

extern const struct SongAttributes {
	__unsafe_unretained NSString *itunesTrackURI;
	__unsafe_unretained NSString *name;
	__unsafe_unretained NSString *originalArtist;
	__unsafe_unretained NSString *spotifyTrackURI;
} SongAttributes;

extern const struct SongRelationships {
	__unsafe_unretained NSString *inSet;
} SongRelationships;

@class Set;

@interface SongID : NSManagedObjectID {}
@end

@interface _Song : NSManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
@property (nonatomic, readonly, strong) SongID* objectID;

@property (nonatomic, strong) NSString* itunesTrackURI;

//- (BOOL)validateItunesTrackURI:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* name;

//- (BOOL)validateName:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* originalArtist;

//- (BOOL)validateOriginalArtist:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* spotifyTrackURI;

//- (BOOL)validateSpotifyTrackURI:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) Set *inSet;

//- (BOOL)validateInSet:(id*)value_ error:(NSError**)error_;

@end

@interface _Song (CoreDataGeneratedPrimitiveAccessors)

- (NSString*)primitiveItunesTrackURI;
- (void)setPrimitiveItunesTrackURI:(NSString*)value;

- (NSString*)primitiveName;
- (void)setPrimitiveName:(NSString*)value;

- (NSString*)primitiveOriginalArtist;
- (void)setPrimitiveOriginalArtist:(NSString*)value;

- (NSString*)primitiveSpotifyTrackURI;
- (void)setPrimitiveSpotifyTrackURI:(NSString*)value;

- (Set*)primitiveInSet;
- (void)setPrimitiveInSet:(Set*)value;

@end
