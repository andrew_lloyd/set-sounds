#import "_Setlist.h"
#import "Song.h"

@interface Setlist : _Setlist {}

- (void)populateWithJSON:(NSDictionary*)json inContext:(NSManagedObjectContext*)context;

- (BOOL)containsSongs;

- (NSString*)setlistAsString;

- (NSString*)dateDisplayString;

- (NSArray*)arrayOfAllSongs;

- (BOOL)allSongsCheckedForSpotifyURI;

- (NSArray*)arrayOfAllSongsURIs;

- (Song*)songWithName:(NSString*)songSearch;

@end
