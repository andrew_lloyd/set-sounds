// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to Set.h instead.

#import <CoreData/CoreData.h>

extern const struct SetAttributes {
	__unsafe_unretained NSString *encore;
} SetAttributes;

extern const struct SetRelationships {
	__unsafe_unretained NSString *containsSongs;
	__unsafe_unretained NSString *inSetlist;
} SetRelationships;

@class Song;
@class Setlist;

@interface SetID : NSManagedObjectID {}
@end

@interface _Set : NSManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
@property (nonatomic, readonly, strong) SetID* objectID;

@property (nonatomic, strong) NSNumber* encore;

@property (atomic) BOOL encoreValue;
- (BOOL)encoreValue;
- (void)setEncoreValue:(BOOL)value_;

//- (BOOL)validateEncore:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSOrderedSet *containsSongs;

- (NSMutableOrderedSet*)containsSongsSet;

@property (nonatomic, strong) Setlist *inSetlist;

//- (BOOL)validateInSetlist:(id*)value_ error:(NSError**)error_;

@end

@interface _Set (ContainsSongsCoreDataGeneratedAccessors)
- (void)addContainsSongs:(NSOrderedSet*)value_;
- (void)removeContainsSongs:(NSOrderedSet*)value_;
- (void)addContainsSongsObject:(Song*)value_;
- (void)removeContainsSongsObject:(Song*)value_;

- (void)insertObject:(Song*)value inContainsSongsAtIndex:(NSUInteger)idx;
- (void)removeObjectFromContainsSongsAtIndex:(NSUInteger)idx;
- (void)insertContainsSongs:(NSArray *)value atIndexes:(NSIndexSet *)indexes;
- (void)removeContainsSongsAtIndexes:(NSIndexSet *)indexes;
- (void)replaceObjectInContainsSongsAtIndex:(NSUInteger)idx withObject:(Song*)value;
- (void)replaceContainsSongsAtIndexes:(NSIndexSet *)indexes withContainsSongs:(NSArray *)values;

@end

@interface _Set (CoreDataGeneratedPrimitiveAccessors)

- (NSNumber*)primitiveEncore;
- (void)setPrimitiveEncore:(NSNumber*)value;

- (BOOL)primitiveEncoreValue;
- (void)setPrimitiveEncoreValue:(BOOL)value_;

- (NSMutableOrderedSet*)primitiveContainsSongs;
- (void)setPrimitiveContainsSongs:(NSMutableOrderedSet*)value;

- (Setlist*)primitiveInSetlist;
- (void)setPrimitiveInSetlist:(Setlist*)value;

@end
