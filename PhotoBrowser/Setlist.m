#import "Setlist.h"
#import "Set.h"
#import "Venue.h"

@interface Setlist ()

// Private interface goes here.

@end

@implementation Setlist

// Custom logic goes here.
- (void)populateWithJSON:(NSDictionary*)json inContext:(NSManagedObjectContext*)context
{
    self.artistName = [json valueForKeyPath:@"artist.name"];
    self.setlistURL = [json valueForKey:@"url"];
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"dd-MM-yyyy"];
    [dateFormatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"GMT+0:00"]];
    
    self.date = [dateFormatter dateFromString:[json valueForKeyPath:@"eventDate"]];
    
    Venue *venue = [NSEntityDescription
                insertNewObjectForEntityForName:@"Venue"
                inManagedObjectContext:context];
    
    
    NSDictionary *venueJSON = [json valueForKeyPath:@"venue"];
    [venue populateWithJSON:venueJSON inContext:context];
    self.inVenue = venue;
    
    id sets = [json valueForKeyPath:@"sets"];
    if ([sets isKindOfClass:[NSString class]])
    {
        if ([sets isEqualToString:@""])
        {
            return;
        }
    }
    else
    {
        NSDictionary *sets = [json valueForKeyPath:@"sets.set"];
        
        if (sets.count > 1)
        {
            for (NSDictionary *setJSON in sets)
            {
                //check set contains songs
                if ([setJSON isKindOfClass:[NSDictionary class]])
                {
                    Set *set = [NSEntityDescription
                                insertNewObjectForEntityForName:@"Set"
                                inManagedObjectContext:context];
                    
                    [set populateWithJSON:setJSON inContext:context];
                    [self.containsSetsSet addObject:set];
                }
            }
        }
        else
        {
            Set *set = [NSEntityDescription
                        insertNewObjectForEntityForName:@"Set"
                        inManagedObjectContext:context];
            
            [set populateWithJSON:sets inContext:context];
            [self.containsSetsSet addObject:set];
        }
    }
}

- (BOOL)containsSongs
{
    for (Set *set in self.containsSets)
    {
        if (set.containsSongs.count > 0)
        {
            return true;
        }
    }
    
    return false;
}


- (NSString*)setlistAsString
{
    NSString *result = @"";
    
    for (Set *set in self.containsSets)
    {
        result = [NSString stringWithFormat:@"%@\n", result];
        
        if (set.encoreValue)
        {
            NSInteger encoreNumber = [self numberOfOccuruncesOfString:@"Encore" inString:result];
            
            if (encoreNumber > 0)
            {
                result = [NSString stringWithFormat:@"%@\nEncore %ld:", result, (long)encoreNumber + 1];
            }
            else
            {
               result = [NSString stringWithFormat:@"%@\nEncore:", result];
            }
        }
        
        for (Song *song in set.containsSongs)
        {
            result = [NSString stringWithFormat:@"%@\n%@", result, song.name];
        }
    }
    
    while ([result hasPrefix:@"\n"])
    {
        result = [result substringFromIndex:1];
    }
    
    return result;
}

- (NSInteger)numberOfOccuruncesOfString:(NSString*)stringQuery inString:(NSString*)string
{
    NSInteger strCount = [string length] - [[string stringByReplacingOccurrencesOfString:stringQuery withString:@""] length];
    strCount /= [stringQuery length];
    
    return strCount;
}

- (NSString*)dateDisplayString
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
    [dateFormatter setDateFormat:@"EEEE, dd MMM yyyy"];
    
    return [dateFormatter stringFromDate:self.date];
}

- (NSArray*)arrayOfAllSongs
{
    NSMutableArray *result = [[NSMutableArray alloc] initWithArray:@[]];
    
    for (Set *set in self.containsSets)
    {
        for (Song *song in set.containsSongs)
        {
            [result addObject:song];
        }
    }
    
    return result;
}

- (BOOL)allSongsCheckedForSpotifyURI
{
    for (Set *set in self.containsSets)
    {
        for (Song *song in set.containsSongs)
        {
            if (song.spotifyTrackURI.length == 0)
            {
                return NO;
            }
        }
    }
    
    return YES;
}

- (NSArray*)arrayOfAllSongsURIs
{
    NSMutableArray *result = [[NSMutableArray alloc] initWithArray:@[]];
    
    for (Song *song in [self arrayOfAllSongs])
    {
        if (song.spotifyTrackURI.length > 0 && ![song.spotifyTrackURI isEqualToString:@"notAvailable"])
        {
            [result addObject:[NSURL URLWithString:song.spotifyTrackURI]];
        }
    }
    
    return result;
}

- (Song*)songWithName:(NSString*)songSearch
{
    for (Song *song in [self arrayOfAllSongs])
    {
        //take out spaces for better string comparison results
        NSString *songS = [songSearch.uppercaseString stringByReplacingOccurrencesOfString:@" " withString:@""];
        
        NSString *songN = [song.name.uppercaseString stringByReplacingOccurrencesOfString:@" " withString:@""];
        
        if ([songS containsString:songN])
        {
            return song;
        }
    }
    
    return nil;
}

@end
