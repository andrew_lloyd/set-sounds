//
//  DateComparison.h
//  PhotoBrowser
//
//  Created by Andrew Lloyd on 28/03/2014.
//  Copyright (c) 2014 Andrew Lloyd. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDate (SameDay)

- (BOOL)isSameDayAsDate:(NSDate*)otherDate;

@end

