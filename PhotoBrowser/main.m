//
//  main.m
//  PhotoBrowser
//
//  Created by Andrew Lloyd on 06/03/2014.
//  Copyright (c) 2014 Andrew Lloyd. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "PhotoBrowserAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([PhotoBrowserAppDelegate class]));
    }
}
