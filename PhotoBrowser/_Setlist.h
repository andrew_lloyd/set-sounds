// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to Setlist.h instead.

#import <CoreData/CoreData.h>

extern const struct SetlistAttributes {
	__unsafe_unretained NSString *artistName;
	__unsafe_unretained NSString *date;
	__unsafe_unretained NSString *setlistURL;
} SetlistAttributes;

extern const struct SetlistRelationships {
	__unsafe_unretained NSString *containsSets;
	__unsafe_unretained NSString *inVenue;
} SetlistRelationships;

@class Set;
@class Venue;

@interface SetlistID : NSManagedObjectID {}
@end

@interface _Setlist : NSManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
@property (nonatomic, readonly, strong) SetlistID* objectID;

@property (nonatomic, strong) NSString* artistName;

//- (BOOL)validateArtistName:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSDate* date;

//- (BOOL)validateDate:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* setlistURL;

//- (BOOL)validateSetlistURL:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSOrderedSet *containsSets;

- (NSMutableOrderedSet*)containsSetsSet;

@property (nonatomic, strong) Venue *inVenue;

//- (BOOL)validateInVenue:(id*)value_ error:(NSError**)error_;

@end

@interface _Setlist (ContainsSetsCoreDataGeneratedAccessors)
- (void)addContainsSets:(NSOrderedSet*)value_;
- (void)removeContainsSets:(NSOrderedSet*)value_;
- (void)addContainsSetsObject:(Set*)value_;
- (void)removeContainsSetsObject:(Set*)value_;

- (void)insertObject:(Set*)value inContainsSetsAtIndex:(NSUInteger)idx;
- (void)removeObjectFromContainsSetsAtIndex:(NSUInteger)idx;
- (void)insertContainsSets:(NSArray *)value atIndexes:(NSIndexSet *)indexes;
- (void)removeContainsSetsAtIndexes:(NSIndexSet *)indexes;
- (void)replaceObjectInContainsSetsAtIndex:(NSUInteger)idx withObject:(Set*)value;
- (void)replaceContainsSetsAtIndexes:(NSIndexSet *)indexes withContainsSets:(NSArray *)values;

@end

@interface _Setlist (CoreDataGeneratedPrimitiveAccessors)

- (NSString*)primitiveArtistName;
- (void)setPrimitiveArtistName:(NSString*)value;

- (NSDate*)primitiveDate;
- (void)setPrimitiveDate:(NSDate*)value;

- (NSString*)primitiveSetlistURL;
- (void)setPrimitiveSetlistURL:(NSString*)value;

- (NSMutableOrderedSet*)primitiveContainsSets;
- (void)setPrimitiveContainsSets:(NSMutableOrderedSet*)value;

- (Venue*)primitiveInVenue;
- (void)setPrimitiveInVenue:(Venue*)value;

@end
