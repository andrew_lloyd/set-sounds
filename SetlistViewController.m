//
//  SetlistViewController.m
//  PhotoBrowser
//
//  Created by Andrew Lloyd on 01/04/2014.
//  Copyright (c) 2014 Andrew Lloyd. All rights reserved.
//

#import "SetlistViewController.h"
#import "SetlistGrabber.h"
#import "SpotifyPlayerViewController.h"
#import "PlaylistAddedConfirmationViewController.h"
#import <RestKit/Restkit.h>
#import "Venue.h"
#import <Spotify/Spotify.h>
#import "Config.h"
#import <AVKit/AVKit.h>
#import <AVFoundation/AVFoundation.h>
#import <Spotify/SPTDiskCache.h>
#import "SpotifyServices.h"
#import "Config.h"

#import <MediaPlayer/MPNowPlayingInfoCenter.h>
#import <MediaPlayer/MPMediaItem.h>
#import <AVFoundation/AVFoundation.h>

@interface SetlistViewController () <UIAlertViewDelegate, SPTAudioStreamingDelegate, SPTAudioStreamingPlaybackDelegate>

@property (atomic, readwrite) SPTAuthViewController *authViewController;
//@property (atomic, readwrite) BOOL firstLoad;

@property (nonatomic) NSString *dayName;
@property (nonatomic) AVPlayer *player;

@property (nonatomic) BOOL addSetTaskSet;
@property (nonatomic) BOOL playSetTaskSet;

@property (nonatomic) BOOL setlistSetToPlayer;

@property (nonatomic) NSTimeInterval currentPlaybackTime;

@end

@implementation SetlistViewController

@synthesize theArtistLabel, theVenueLabel, theDateLabel, theMainSetField, dateOfShow, artist, venue;

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self setUpCoreDataStack];
    
    [self.setlistSpinner setHidden:NO];
    [self.setlistSpinner startAnimating];
    
    [self.playerActivityIndicator setHidden:YES];
    
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
    
    [self roundSpotifyButtonCorners];
    
    //randomise background image
    NSInteger randomNumber = arc4random_uniform(4);
    NSString *imagename = [NSString stringWithFormat:@"%li", (long)randomNumber];
    self.backgroundImageView.image = [UIImage imageNamed:imagename];
    
    self.setlistImage.image = [UIImage imageNamed:@"setlist_image"];
    
    //dyanmic constraints to make the textView fit the setlist image depending on screen size/device
    CGFloat imageHeight = self.setlistImage.bounds.size.height;
    self.topGapHeight.constant = imageHeight * 0.11;
    self.bottomGapHeight.constant = imageHeight * 0.07;
    CGFloat imageWidth = self.setlistImage.bounds.size.width;
    self.leftGapHeight.constant = imageWidth * 0.06;
    self.rightGapHeight.constant = imageWidth * 0.12;
    
    if (_spotifyPlayer)
    {
        _spotifyPlayer.playbackDelegate = self;
    }
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
    if(_setlist == nil){
        [self fetchSet];
    }
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    self.backgroundImageView.image = nil;
    [self.spotifyPlayer stop:^(NSError *error) {
        nil;
    }];
    
    [self.delegate sendSessionToViewController:_session withPlayer:self.spotifyPlayer];
}

- (void)setSetlist
{
    if(_setlist != nil)
    {
        [self.setlistSpinner stopAnimating];
        [self.setlistSpinner setHidden:YES];
        
        theArtistLabel.text = _setlist.artistName;
        theVenueLabel.text = _setlist.inVenue.name;
        theDateLabel.text = [_setlist dateDisplayString];
        
        theMainSetField.text = _setlist.setlistAsString;
    }
    
    if([theMainSetField.text isEqualToString:@""]){
        UIAlertView *noSetAlert = [[UIAlertView alloc] initWithTitle:@"No Results"
                                                             message:@"Sorry, we don't have the setlist for that show available."
                                                            delegate:self
                                                   cancelButtonTitle:@"OK"
                                                   otherButtonTitles: nil];
        [noSetAlert show];
    }
    
    [self.setlistSpinner stopAnimating];
    [self.setlistSpinner setHidden:YES];
}

- (void)fetchSet
{
    [self.setlistSpinner startAnimating]; // start the spinner
    
    SetlistGrabber *setGrabber = [[SetlistGrabber alloc]init];

    NSString *artistString = artist;
    artistString = [artistString stringByReplacingOccurrencesOfString:@" " withString:@"+"];
    [setGrabber fetchSetlistofArtist:artistString atVenue:venue onDate:dateOfShow inContext:self.managedObjectContext];
    
    NSFetchRequest *fetchSetRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Setlist"
                                              inManagedObjectContext:self.managedObjectContext];
    NSSortDescriptor *sortDesc = [NSSortDescriptor sortDescriptorWithKey:@"date" ascending:NO];
    [fetchSetRequest setSortDescriptors:@[sortDesc]];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"containsSets.@count > 0", self];
    [fetchSetRequest setEntity:entity];
    [fetchSetRequest setPredicate:predicate];
    
    NSArray *fetchedObjects = [self.managedObjectContext executeFetchRequest:fetchSetRequest error:nil]
    ;
    _setlist = fetchedObjects.firstObject;
    [self setSetlist];
    
    if (_session)
    {
        [self fetchSetURIsWithSuccess:^{
            [self.setlistSpinner stopAnimating]; // stop the spinner
        } andFailure:^(NSError *error) {
            //do nothing
            [self.setlistSpinner stopAnimating]; // stop the spinner
        }];
    }
    else
    {
        [self.setlistSpinner stopAnimating]; // stop the spinner
    }
}

#pragma mark - spotify player intergration

- (BOOL)prefersStatusBarHidden {
    return NO;
}

-(void)sessionUpdatedNotification:(NSNotification *)notification {
    if(self.navigationController.topViewController == self) {
        SPTAuth *auth = [SPTAuth defaultInstance];
        if (auth.session && [auth.session isValid]) {
            //[self performSegueWithIdentifier:@"ShowPlayer" sender:nil];
        }
    }
}

-(void)showPlayer {
//    self.firstLoad = NO;
    
    PlaylistAddedConfirmationViewController *viewController = (PlaylistAddedConfirmationViewController *)[self.storyboard instantiateViewControllerWithIdentifier:@"PlaylistAddedConfirmation"];
    viewController.setlist = self.setlist;
    viewController.managedObjectContext = self.managedObjectContext;
    
    if (self.setlist != nil)
    {
        //[self.navigationController pushViewController:viewController animated:NO];
        //[self presentViewController:viewController animated:NO completion:nil];
        [viewController showInView:self.view animated:YES];
    }
    
//        SpotifyPlayerViewController *viewController = (SpotifyPlayerViewController *)[self.storyboard instantiateViewControllerWithIdentifier:@"SpotifyPlayer"];
//        viewController.artist = self.artist;
//        viewController.venue = self.venue;
//        viewController.dateOfShow = self.dayName;
//        viewController.setlist = self.setlist;
//    
//        if (self.setlist != nil)
//        {
//            //[self.navigationController pushViewController:viewController animated:NO];
//            [viewController showInView:self.view animated:NO];
//        }
}

- (void)addSetlistPlaylistToUsersAccount
{
    PlaylistAddedConfirmationViewController *viewController = (PlaylistAddedConfirmationViewController *)[self.storyboard instantiateViewControllerWithIdentifier:@"PlaylistAddedConfirmation"];
    viewController.setlist = self.setlist;
    viewController.managedObjectContext = self.managedObjectContext;
    
    if (self.setlist != nil)
    {
        //[self.navigationController pushViewController:viewController animated:NO];
        //[self presentViewController:viewController animated:NO completion:nil];
        [viewController showInView:self.view animated:YES];
    }
}

- (void)authenticationViewController:(SPTAuthViewController *)viewcontroller didFailToLogin:(NSError *)error {
    NSLog(@"*** Failed to log in: %@", error);
}

- (void)authenticationViewController:(SPTAuthViewController *)viewcontroller didLoginWithSession:(SPTSession *)session {
    self.session = session;
    
    [self fetchSetURIsWithSuccess:^{
        if (self.addSetTaskSet)
        {
            [self addSetlistPlaylistToUsersAccount];
            self.addSetTaskSet = NO;
        }
        
        if (self.playSetTaskSet)
        {
            [self playButtonPressed:nil];
            self.playSetTaskSet = NO;
        }
        
    } andFailure:^(NSError *error) {
        //do nothing
    }];
}

- (void)authenticationViewControllerDidCancelLogin:(SPTAuthViewController *)authenticationViewController {
    //self.statusLabel.text = @"Login cancelled.";
}

- (void)openLoginPage {
    //self.statusLabel.text = @"Logging in...";
    
    self.authViewController = [SPTAuthViewController authenticationViewController];
    self.authViewController.delegate = self;
    self.authViewController.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    self.authViewController.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    
    self.modalPresentationStyle = UIModalPresentationCurrentContext;
    self.definesPresentationContext = YES;
    
    [self presentViewController:self.authViewController animated:NO completion:nil];
}


- (void)renewTokenAndShowPlayer {
    //self.statusLabel.text = @"Refreshing token...";
    SPTAuth *auth = [SPTAuth defaultInstance];
    
    [auth renewSession:auth.session callback:^(NSError *error, SPTSession *session) {
        auth.session = session;
        
        if (error) {
            //self.statusLabel.text = @"Refreshing token failed.";
            NSLog(@"*** Error renewing session: %@", error);
            return;
        }
        
        [self showPlayer];
    }];
}

- (void)roundSpotifyButtonCorners
{
    //round corners of check box
    // Create the path
    UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:self.spotifyButton.bounds
                                                   byRoundingCorners:UIRectCornerAllCorners
                                                         cornerRadii:CGSizeMake(14.0, 14.0)];
    
    // Create the shape layer and set its path
    CAShapeLayer *maskLayer = [CAShapeLayer layer];
    maskLayer.frame = self.spotifyButton.bounds;
    maskLayer.path = maskPath.CGPath;
    // Set the newly created shape layer as the mask for the image view's layer
    self.spotifyButton.layer.mask = maskLayer;
}

- (void)viewWillAppear:(BOOL)animated {
    SPTAuth *auth = [SPTAuth defaultInstance];
    
    // Check if we have a token at all
    if (auth.session == nil) {
        //self.statusLabel.text = @"";
        return;
    }
    
    // Check if it's still valid
//    if ([auth.session isValid] && self.firstLoad) {
//        // It's still valid, show the player.
//        //[self showPlayer];
//        return;
//    }
    
    // Oh noes, the token has expired, if we have a token refresh service set up, we'll call tat one.
    //self.statusLabel.text = @"Token expired.";
    if (auth.hasTokenRefreshService) {
        //[self renewTokenAndShowPlayer];
        return;
    }
    
    // Else, just show login dialog
}



//- (IBAction)clearCookiesClicked:(id)sender {
//    self.authViewController = [SPTAuthViewController authenticationViewController];
//    [self.authViewController clearCookies:nil];
//    //self.statusLabel.text = @"Cookies cleared.";
//}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)setUpCoreDataStack
{
    NSManagedObjectModel *model = [NSManagedObjectModel mergedModelFromBundles:[NSBundle allBundles]];
    
    NSPersistentStoreCoordinator *psc = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:model];
    
    NSURL *url = [[[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject] URLByAppendingPathComponent:@"Database.sqlite"];
    
    [psc addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:url options:nil error:nil];
    
    self.managedObjectContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSMainQueueConcurrencyType];
    
    self.managedObjectContext.persistentStoreCoordinator = psc;
}

#pragma mark - button presses

- (IBAction)addSetToSpotify:(id)sender {
    if (_session == nil)
    {
        [self openLoginPage];
        self.addSetTaskSet = true;
    }
    else
    {
        if ([_setlist arrayOfAllSongsURIs].count == 0)
        {
            [self presentNoSongsAvailableInSpotifyAlert];
        }
        else
        {
            [self addSetlistPlaylistToUsersAccount];
        }
    }
}

- (IBAction)playButtonPressed:(id)sender {
    if (_session) {
        
        SPTAuth *auth = [SPTAuth defaultInstance];
        if (self.spotifyPlayer == nil)
        {
            [self.playerActivityIndicator setHidden:NO];
            [self.playerActivityIndicator startAnimating];
            [self.playButton setHidden:YES];
            [self.skipBackButton setHidden:YES];
            [self.skipForwardButton setHidden:YES];
            
            self.spotifyPlayer = [[SPTAudioStreamingController alloc] initWithClientId:auth.clientID];
            self.spotifyPlayer.playbackDelegate = self;
            self.spotifyPlayer.diskCache = [[SPTDiskCache alloc] initWithCapacity:1024 * 1024 * 64];
            [[UIApplication sharedApplication] beginReceivingRemoteControlEvents];
            
            [self.spotifyPlayer loginWithSession:auth.session callback:^(NSError *error)
             {
                 if ([_setlist arrayOfAllSongsURIs].count == 0)
                 {
                     [self presentNoSongsAvailableInSpotifyAlert];
                     [self.playerActivityIndicator setHidden:YES];
                     [self.playButton setHidden:NO];
                     [self.skipBackButton setHidden:NO];
                     [self.skipForwardButton setHidden:NO];
                 }
                 else
                 {
                     [self.spotifyPlayer playURIs:[_setlist arrayOfAllSongsURIs] withOptions:nil callback:nil];
                     [self.playerActivityIndicator setHidden:YES];
                     [self.playButton setHidden:NO];
                     [self.skipBackButton setHidden:NO];
                     [self.skipForwardButton setHidden:NO];
                     [self.playButton setImage:[UIImage imageNamed:@"pause-button"] forState:UIControlStateNormal];
                     self.setlistSetToPlayer = YES;
                 }
             }];
        }
        else if(self.setlistSetToPlayer)
        {
            //playlist has already been played atleast once
            [self pausePlay];
        }
        else
        {
            //if a new set is loaded we need to reset spotify player queue
            SPTPlayOptions *playOptions = [[SPTPlayOptions alloc] init];
            playOptions.trackIndex = 0;
            
            if ([_setlist arrayOfAllSongsURIs].count == 0)
            {
                [self presentNoSongsAvailableInSpotifyAlert];
                [self.playerActivityIndicator setHidden:YES];
                [self.playButton setHidden:NO];
                [self.skipBackButton setHidden:NO];
                [self.skipForwardButton setHidden:NO];
            }
            else
            {
                [self.spotifyPlayer playURIs:[_setlist arrayOfAllSongsURIs] withOptions:playOptions callback:nil];
                [self.playerActivityIndicator setHidden:YES];
                [self.playButton setHidden:NO];
                self.setlistSetToPlayer = YES;
                [self.playButton setImage:[UIImage imageNamed:@"pause-button"] forState:UIControlStateNormal];
            }
        }
    }
    else
    {
        [self openLoginPage];
        self.playSetTaskSet = true;
    }
}

- (IBAction)setlistSourceButtonPressed:(id)sender
{
    NSURL *url = [NSURL URLWithString:_setlist.setlistURL];
    [[UIApplication sharedApplication] openURL:url];
}

- (void) audioStreaming:(SPTAudioStreamingController *)audioStreaming didChangeToTrack:(NSDictionary *)trackMetadata {
    NSLog(@"track changed = %@", [trackMetadata valueForKey:SPTAudioStreamingMetadataTrackURI]);
    [self updateUI];
    [self getSongInfo];
}

- (void)fetchSetURIsWithSuccess:(void (^) ())success
                     andFailure:(void (^) (NSError *error))failure
{
    [self.setlistSpinner startAnimating];
    
    SpotifyServices *spotifySer = [[SpotifyServices alloc] init];
    [spotifySer fetchSongURIsForSetlist:_setlist withSuccess:^() {
        success();
        
    } andFailure:^(NSError *error) {
        
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"There has been a problem playing the setlist."
                                                                       message:error.localizedDescription
                                                                preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                              handler:^(UIAlertAction * action) {
                                                                  //handle alert ok button
                                                              }];
        
        [alert addAction:defaultAction];
        [self presentViewController:alert animated:YES completion:nil];
    }];
}

- (void)updateUI
{
    [SPTTrack trackWithURI:self.spotifyPlayer.currentTrackURI
                   session:_session
                  callback:^(NSError *error, SPTTrack *track) {
                      
                      Song *songInSet = [_setlist songWithName:track.name];
                      
                      if (songInSet)
                      {
                           NSMutableAttributedString *attibutedStringSet = [[NSMutableAttributedString alloc] initWithString:_setlist.setlistAsString];
                          
                          [attibutedStringSet addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"AmericanTypewriter" size:16.0] range:[attibutedStringSet.string.uppercaseString rangeOfString:attibutedStringSet.string.uppercaseString]];
                          [attibutedStringSet addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"AmericanTypewriter-Bold" size:18.0] range:[attibutedStringSet.string.uppercaseString rangeOfString:songInSet.name.uppercaseString]];
                          
                          NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc]init] ;
                          [paragraphStyle setAlignment:NSTextAlignmentCenter];
                          [attibutedStringSet addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, [attibutedStringSet length])];
                          
                          theMainSetField.attributedText = attibutedStringSet;
                      }
                      else
                      {
                          NSMutableAttributedString *attibutedStringSet = [[NSMutableAttributedString alloc] initWithString:_setlist.setlistAsString];
                          
                          [attibutedStringSet addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"AmericanTypewriter" size:16.0] range:[attibutedStringSet.string.uppercaseString rangeOfString:attibutedStringSet.string.uppercaseString]];
                          
                          NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc]init] ;
                          [paragraphStyle setAlignment:NSTextAlignmentCenter];
                          [attibutedStringSet addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, [attibutedStringSet length])];
                          
                          theMainSetField.attributedText = attibutedStringSet;
                      }
                  }];
}

- (IBAction)skipForward:(id)sender
{
    if ([_setlist arrayOfAllSongsURIs].count == 0)
    {
        [self presentNoSongsAvailableInSpotifyAlert];
    }
    else
    {
        [self.spotifyPlayer skipNext:nil];
    }
}

- (IBAction)skipBackward:(id)sender
{
    if ([_setlist arrayOfAllSongsURIs].count == 0)
    {
        [self presentNoSongsAvailableInSpotifyAlert];
    }
    else
    {
        [self.spotifyPlayer skipPrevious:nil];
    }
}

- (void)pausePlay
{
    [self.spotifyPlayer setIsPlaying:!self.spotifyPlayer.isPlaying callback:nil];
    
    if ([_spotifyPlayer isPlaying])
    {
        //pause playing
        theMainSetField.text = _setlist.setlistAsString;
        [self.playButton setImage:[UIImage imageNamed:@"play-button"] forState:UIControlStateNormal];
        
        self.currentPlaybackTime = _spotifyPlayer.currentPlaybackPosition;
    }
    else
    {
        //start playing
        [self updateUI];
        [self.playButton setImage:[UIImage imageNamed:@"pause-button"] forState:UIControlStateNormal];
        
        MPNowPlayingInfoCenter *center = [MPNowPlayingInfoCenter defaultCenter];
        NSMutableDictionary *playingInfo = [NSMutableDictionary dictionaryWithDictionary:center.nowPlayingInfo];
        [playingInfo setObject:[NSNumber numberWithFloat:self.currentPlaybackTime] forKey:MPNowPlayingInfoPropertyElapsedPlaybackTime];
        center.nowPlayingInfo = playingInfo;
    }
}

- (void)getSongInfo
{
    [SPTTrack trackWithURI:self.spotifyPlayer.currentTrackURI
                   session:self.session
                  callback:^(NSError *error, SPTTrack *track) {
                      
                      Class playingInfoCenter = NSClassFromString(@"MPNowPlayingInfoCenter");
                      
                      if (playingInfoCenter && track) {
                          
                          
                          NSMutableDictionary *songInfo = [[NSMutableDictionary alloc] init];
                          
                          SPTPartialArtist *artist = [track.artists objectAtIndex:0];
                          
                          [songInfo setObject:track.name forKey:MPMediaItemPropertyTitle];
                          [songInfo setObject:artist.name forKey:MPMediaItemPropertyArtist];
                          [songInfo setObject:track.album.name forKey:MPMediaItemPropertyAlbumTitle];
                          [songInfo setValue:@(track.duration) forKey:MPMediaItemPropertyPlaybackDuration];
                          
                          NSURL *imageURL = track.album.largestCover.imageURL;
                          if (imageURL == nil) {
                              NSLog(@"Album %@ doesn't have any images!", track.album);
                              return;
                          }
                          
                          // Pop over to a background queue to load the image over the network.
                          dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                              NSError *error = nil;
                              UIImage *image = nil;
                              NSData *imageData = [NSData dataWithContentsOfURL:imageURL options:0 error:&error];
                              
                              if (imageData != nil) {
                                  image = [UIImage imageWithData:imageData];
                              }
                              
                              
                              // …and back to the main queue to display the image.
                              dispatch_async(dispatch_get_main_queue(), ^{
                                  MPMediaItemArtwork *albumArt = [[MPMediaItemArtwork alloc] initWithImage: image];
                                  [songInfo setObject:albumArt forKey:MPMediaItemPropertyArtwork];
                                  [[MPNowPlayingInfoCenter defaultCenter] setNowPlayingInfo:songInfo];
                                  if (image == nil) {
                                      NSLog(@"Couldn't load cover image with error: %@", error);
                                      return;
                                  }
                                  
                                  self.backgroundImageView.image = image;
                              });
                          });
                      }
                  }];
}

- (void)remoteControlReceivedWithEvent:(UIEvent *)event
{
    if (event.type == UIEventTypeRemoteControl) {
        
        switch (event.subtype) {
            case UIEventSubtypeRemoteControlTogglePlayPause:
                [self pausePlay];
                break;
            case UIEventSubtypeRemoteControlPause:
                [self pausePlay];
                break;
            case UIEventSubtypeRemoteControlStop:
                [self pausePlay];
                break;
            case UIEventSubtypeRemoteControlPlay:
                [self playButtonPressed:nil];
                break;
            case UIEventSubtypeRemoteControlPreviousTrack:
                [self skipBackward:nil];
                break;
            case UIEventSubtypeRemoteControlNextTrack:
                [self skipForward:nil];
                break;
            default:
                break;
        }
    }
}

- (void)presentNoSongsAvailableInSpotifyAlert
{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Sorry"
                                                                   message:@"We can't find any of these songs in the Spotify library"
                                                            preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                          handler:^(UIAlertAction * action) {
                                                          }];
    
    [alert addAction:defaultAction];
    [self presentViewController:alert animated:YES completion:nil];
}

-(void)audioStreaming:(SPTAudioStreamingController *)audioStreaming didChangePlaybackStatus:(BOOL)isPlaying
{
    if (isPlaying)
    {
        [self.playButton setImage:[UIImage imageNamed:@"pause-button"] forState:UIControlStateNormal];
    }
    else
    {
        [self.playButton setImage:[UIImage imageNamed:@"play-button"] forState:UIControlStateNormal];
    }

}

@end
