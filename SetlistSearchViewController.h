//
//  SetlistSearchViewController.h
//  PhotoBrowser
//
//  Created by Andrew Lloyd on 01/04/2014.
//  Copyright (c) 2014 Andrew Lloyd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SetlistViewController.h"
#import <Spotify/Spotify.h>

@interface SetlistSearchViewController : UIViewController <UITextFieldDelegate>

- (IBAction)searchBtn:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *infoButton;
@property (weak, nonatomic) IBOutlet UIImageView *backgroundImageView;
@property (weak, nonatomic) IBOutlet UIView *popupView;
@property (weak, nonatomic) IBOutlet UIView *popupBorderView;

@property (strong, nonatomic) IBOutlet UITextField *artistField;
@property (strong, nonatomic) IBOutlet UITextField *venueField;
@property (weak, nonatomic) IBOutlet UITextField *dateField;

@property (weak, nonatomic) IBOutlet UIButton *spotifyLogInButton;

@property (nonatomic, strong) SPTSession *session;
@property (nonatomic, strong) SPTAudioStreamingController *spotifyPlayer;

- (void)prepareSetlistViewController:(SetlistViewController*)svc;

@end
